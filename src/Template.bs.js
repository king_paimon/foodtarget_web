// Generated by BUCKLESCRIPT VERSION 4.0.1, PLEASE EDIT WITH CARE

import * as React from "react";
import * as LogoSvg from "./logo.svg";
import * as Pervasives from "bs-platform/lib/es6/pervasives.js";
import * as ReasonReact from "reason-react/src/ReasonReact.js";
import * as StarIconSvg from "./starIcon.svg";
import * as DeliPackageSvg from "./deliPackage.svg";
import * as InputCloseSvg from "./input-close.svg";
import * as Input_locateSvg from "./input_locate.svg";
import * as Arrow_down_blackSvg from "./arrow_down_black.svg";

((require('./App.css')));

var component = ReasonReact.statelessComponent("Template");

function make() {
  return /* record */[
          /* debugName */component[/* debugName */0],
          /* reactClassInternal */component[/* reactClassInternal */1],
          /* handedOffState */component[/* handedOffState */2],
          /* willReceiveProps */component[/* willReceiveProps */3],
          /* didMount */component[/* didMount */4],
          /* didUpdate */component[/* didUpdate */5],
          /* willUnmount */component[/* willUnmount */6],
          /* willUpdate */component[/* willUpdate */7],
          /* shouldUpdate */component[/* shouldUpdate */8],
          /* render */(function () {
              return React.createElement("div", {
                          className: "container"
                        }, React.createElement("header", {
                              className: "header--content"
                            }, React.createElement("div", {
                                  className: "header--header"
                                }, React.createElement("div", {
                                      className: "header--logo"
                                    }, React.createElement("a", {
                                          href: "#"
                                        }, React.createElement("img", {
                                              className: "header--logoImage",
                                              alt: "logo",
                                              src: LogoSvg
                                            }))), React.createElement("div", {
                                      className: "header--menu"
                                    }, React.createElement("a", {
                                          href: "#"
                                        }, React.createElement("span", {
                                              className: "header--link"
                                            }, "Restaurants")), React.createElement("a", {
                                          href: "#"
                                        }, React.createElement("span", {
                                              className: "header--link"
                                            }, "Delivery"))), React.createElement("div", {
                                      className: "header--button"
                                    }, "Moscow"), React.createElement("button", {
                                      className: "header--button"
                                    }, "Enter")), React.createElement("div", {
                                  className: "header--bottomPad"
                                })), React.createElement("div", {
                              className: "main--wrapper"
                            }, React.createElement("div", {
                                  className: "main--container"
                                }, React.createElement("div", {
                                      className: "main--app"
                                    }, React.createElement("div", {
                                          className: "main--content"
                                        }, React.createElement("div", {
                                              className: "main--promo"
                                            }, React.createElement("div", {
                                                  className: "main--promo--bg"
                                                }), React.createElement("div", {
                                                  className: "main--promo--text"
                                                }, React.createElement("h1", {
                                                      className: "main--promo--title"
                                                    }, "Fastfood delivery"), React.createElement("div", {
                                                      className: "main--promo--addressBlock"
                                                    }, React.createElement("div", {
                                                          className: "main--promo--inputWrap"
                                                        }, React.createElement("div", {
                                                              className: "main--promo--addressControl"
                                                            }, React.createElement("div", {
                                                                  className: "main--promo--addressInput"
                                                                }, React.createElement("div", {
                                                                      className: "react-autosuggest__container"
                                                                    }, React.createElement("input", {
                                                                          className: "main--promo--addressInput-input",
                                                                          placeholder: "Type delivery destination...",
                                                                          type: "text"
                                                                        })), React.createElement("img", {
                                                                      className: "main--promo--adressInput-img",
                                                                      src: Input_locateSvg
                                                                    }), React.createElement("img", {
                                                                      className: "main--promo--adressInputClose-img",
                                                                      src: InputCloseSvg
                                                                    })))), React.createElement("div", {
                                                          className: "main--promo--buttonWrap"
                                                        }, React.createElement("button", {
                                                              className: "main--promo--button"
                                                            }, "Show restaurants"))))), React.createElement("div", {
                                              className: "main--filter"
                                            }, React.createElement("ul", {
                                                  className: "main--filter--list"
                                                }, React.createElement("li", {
                                                      className: "main--filter--item"
                                                    }, React.createElement("a", {
                                                          className: "main--filter--itemLink main--filter--itemLink-active",
                                                          href: "#"
                                                        }, "All")), React.createElement("li", {
                                                      className: "main--filter--item"
                                                    }, React.createElement("a", {
                                                          className: "main--filter--itemLink",
                                                          href: "#"
                                                        }, "Burgers")), React.createElement("li", {
                                                      className: "main--filter--item"
                                                    }, React.createElement("a", {
                                                          className: "main--filter--itemLink",
                                                          href: "#"
                                                        }, "Sushi")), React.createElement("li", {
                                                      className: "main--filter--item"
                                                    }, React.createElement("a", {
                                                          className: "main--filter--itemLink",
                                                          href: "#"
                                                        }, "Pizza")), React.createElement("li", {
                                                      className: "main--filter--item"
                                                    }, React.createElement("a", {
                                                          className: "main--filter--itemLink",
                                                          href: "#"
                                                        }, "Healthy food")), React.createElement("li", {
                                                      className: "main--filter--item"
                                                    }, React.createElement("a", {
                                                          className: "main--filter--itemLink",
                                                          href: "#"
                                                        }, "Pasta")), React.createElement("li", {
                                                      className: "main--filter--item"
                                                    }, React.createElement("a", {
                                                          className: "main--filter--itemLink",
                                                          href: "#"
                                                        }, "Veggy")), React.createElement("li", {
                                                      className: "main--filter--item-more"
                                                    }, React.createElement("span", {
                                                          className: "main--filter--item-moreLink"
                                                        }, "more", React.createElement("img", {
                                                              className: "main--filter--item-more-triandleDown",
                                                              src: Arrow_down_blackSvg
                                                            }))))), React.createElement("div", {
                                              className: "main--content-content"
                                            }, React.createElement("div", {
                                                  className: "main--content--controlsWrapper"
                                                }, React.createElement("div", {
                                                      className: "main--content--controls"
                                                    }, React.createElement("div", {
                                                          className: "main--content--controls_h"
                                                        }, React.createElement("div", {
                                                              className: "react-autosuggest__container"
                                                            }, React.createElement("input", {
                                                                  className: "main--content--controlsInput",
                                                                  placeholder: "Restaurant name, kitchen or dishes",
                                                                  type: "text"
                                                                })), React.createElement("i", {
                                                              className: "fas fa-search icon-search"
                                                            }), React.createElement("img", {
                                                              className: "main--promo--adressInputClose-img",
                                                              src: InputCloseSvg
                                                            })))), React.createElement("div", {
                                                  className: "main--content--restList"
                                                }, React.createElement("h2", {
                                                      className: "main--content--restList-title"
                                                    }, "Restaurants"), React.createElement("ul", {
                                                      className: "main--content--restList-list"
                                                    }, React.createElement("li", {
                                                          className: "main--content--restItem"
                                                        }, React.createElement("a", {
                                                              className: "main--content--restItem-link",
                                                              href: "#"
                                                            }, React.createElement("div", {
                                                                  className: "main--content--restItem-imageContainer"
                                                                }, React.createElement("div", {
                                                                      className: "main--content--restItem-imageWrapper"
                                                                    }, React.createElement("div", {
                                                                          className: "main--content--restItem-imageNodeRoot"
                                                                        }, React.createElement("div", {
                                                                              className: "main--content--restItem-imageNode"
                                                                            })))), React.createElement("h3", {
                                                                  className: "main--content--restItem-name",
                                                                  title: "Online menu of restaurant McDuck"
                                                                }, "McDuck"), React.createElement("div", {
                                                                  className: "main--content--restItem-details"
                                                                }, React.createElement("div", {
                                                                      className: "main--content--restItem-deliveryBadge"
                                                                    }, React.createElement("div", {
                                                                          className: "deliveryBadge"
                                                                        })), React.createElement("div", {
                                                                      className: "main--content--restItem-rating"
                                                                    }, React.createElement("img", {
                                                                          className: "main--content--restItem-starIcon",
                                                                          src: StarIconSvg
                                                                        }), Pervasives.string_of_float(4.9)), React.createElement("div", {
                                                                      className: "main--content--restItem-orderMinPrice"
                                                                    }, React.createElement("img", {
                                                                          className: "main--content--restItem-deliveryPackage",
                                                                          src: DeliPackageSvg
                                                                        }), "Order from ", String(0), " $")), React.createElement("div", {
                                                                  className: "main--content--restItem-restTagsWrapper"
                                                                }, React.createElement("div", {
                                                                      className: "main--content--restItem-restTags"
                                                                    }, React.createElement("span", {
                                                                          className: "main--content--restItem-restTag"
                                                                        }, "Russian"), React.createElement("span", {
                                                                          className: "main--content--restItem-restTag"
                                                                        }, "$$")))))), React.createElement("div", {
                                                      className: "main--content--restList-more"
                                                    }, React.createElement("button", {
                                                          className: "main--content--restList-button"
                                                        }, "Show more restaurants")))))))), React.createElement("footer", {
                              className: "footer--content"
                            }, React.createElement("div", {
                                  className: "footer--footer"
                                }, React.createElement("div", {
                                      className: "footer--copyright"
                                    }, "FastfoodDely, 2018"), React.createElement("ul", {
                                      className: "footer--socials"
                                    }, React.createElement("li", {
                                          className: "footer--socialItem"
                                        }, React.createElement("a", {
                                              className: "footer--socialFB footer--social",
                                              href: "#",
                                              target: "_black"
                                            }, React.createElement("i", {
                                                  className: "fab fa-facebook-f footer--socialItem"
                                                }))), React.createElement("li", {
                                          className: "footer--socialItem"
                                        }, React.createElement("a", {
                                              className: "footer--socialVK footer--social",
                                              href: "#",
                                              target: "_black"
                                            }, React.createElement("i", {
                                                  className: "fab fa-vk footer--socialItem"
                                                }))), React.createElement("li", {
                                          className: "footer--socialItem"
                                        }, React.createElement("a", {
                                              className: "footer--socialInst footer--social",
                                              href: "#",
                                              target: "_black"
                                            }, React.createElement("i", {
                                                  className: "fab fa-instagram footer--socialItem"
                                                })))), React.createElement("div", {
                                      className: "footer--links"
                                    }, React.createElement("a", {
                                          className: "footer--link",
                                          href: "#"
                                        }, "About us"), React.createElement("a", {
                                          className: "footer--link",
                                          href: "#",
                                          target: "_blank"
                                        }, "FAQ"), React.createElement("a", {
                                          className: "footer--link",
                                          href: "#",
                                          target: "_blank"
                                        }, "User Agreement")), React.createElement("div", {
                                      className: "footer--feedback"
                                    }, React.createElement("a", {
                                          href: "#"
                                        }, React.createElement("i", {
                                              className: "fas fa-comment icon-comment"
                                            }), "Feedback")))));
            }),
          /* initialState */component[/* initialState */10],
          /* retainedProps */component[/* retainedProps */11],
          /* reducer */component[/* reducer */12],
          /* subscriptions */component[/* subscriptions */13],
          /* jsElementWrapped */component[/* jsElementWrapped */14]
        ];
}

export {
  component ,
  make ,
  
}
/*  Not a pure module */
