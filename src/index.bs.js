// Generated by BUCKLESCRIPT VERSION 4.0.1, PLEASE EDIT WITH CARE

import * as Template from "./Template.bs.js";
import * as ReactDOMRe from "reason-react/src/ReactDOMRe.js";
import * as ReasonReact from "reason-react/src/ReasonReact.js";
import * as RegisterServiceWorker from "./registerServiceWorker";

((require('./index.css')));

ReactDOMRe.renderToElementWithId(ReasonReact.element(undefined, undefined, Template.make(/* array */[])), "root");

RegisterServiceWorker.default();

export {
  
}
/*  Not a pure module */
