[%bs.raw {|require('./App.css')|}];

[@bs.module] external logo : string = "./logo.svg";
[@bs.module] external inputLocate : string = "./input_locate.svg";
[@bs.module] external inputClose : string = "./input-close.svg";
[@bs.module] external triandleDown : string = "./arrow_down_black.svg";
[@bs.module] external deliveryBadge : string = "./deliveryBadge.svg";
[@bs.module] external starIcon : string = "./starIcon.svg";
[@bs.module] external deliveryPackage : string = "./deliPackage.svg";

let component = ReasonReact.statelessComponent("Template");

let make = _children => {
  ...component,
  render: self =>
    <div className="container">
      <header className="header--content">
        <div className="header--header">
          <div className="header--logo">
            <a href="#">
              <img src=logo className="header--logoImage" alt="logo" />
            </a>
          </div>
          <div className="header--menu">
            <a href="#">
              <span className="header--link">
                (ReasonReact.string("Restaurants"))
              </span>
            </a>
            <a href="#">
              <span className="header--link">
                (ReasonReact.string("Delivery"))
              </span>
            </a>
          </div>
          <div className="header--button">
            (ReasonReact.string("Moscow"))
          </div>
          <button className="header--button">
            (ReasonReact.string("Enter"))
          </button>
        </div>
        <div className="header--bottomPad" />
      </header>
      <div className="main--wrapper">
        <div className="main--container">
          <div className="main--app">
            <div className="main--content">
              <div className="main--promo">
                <div className="main--promo--bg" />
                <div className="main--promo--text">
                  <h1 className="main--promo--title">
                    (ReasonReact.string("Fastfood delivery"))
                  </h1>
                  <div className="main--promo--addressBlock">
                    <div className="main--promo--inputWrap">
                      <div className="main--promo--addressControl">
                        <div className="main--promo--addressInput">
                          <div className="react-autosuggest__container">
                            <input
                              className="main--promo--addressInput-input"
                              placeholder="Type delivery destination..."
                              _type="text"
                            />
                          </div>
                          <img
                            className="main--promo--adressInput-img"
                            src=inputLocate
                          />
                          <img
                            className="main--promo--adressInputClose-img"
                            src=inputClose
                          />
                        </div>
                      </div>
                    </div>
                    <div className="main--promo--buttonWrap">
                      <button className="main--promo--button">
                        (ReasonReact.string("Show restaurants"))
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="main--filter">
                <ul className="main--filter--list">
                  <li className="main--filter--item">
                    <a
                      className="main--filter--itemLink main--filter--itemLink-active"
                      href="#">
                      (ReasonReact.string("All"))
                    </a>
                  </li>
                  <li className="main--filter--item">
                    <a className="main--filter--itemLink" href="#">
                      (ReasonReact.string("Burgers"))
                    </a>
                  </li>
                  <li className="main--filter--item">
                    <a className="main--filter--itemLink" href="#">
                      (ReasonReact.string("Sushi"))
                    </a>
                  </li>
                  <li className="main--filter--item">
                    <a className="main--filter--itemLink" href="#">
                      (ReasonReact.string("Pizza"))
                    </a>
                  </li>
                  <li className="main--filter--item">
                    <a className="main--filter--itemLink" href="#">
                      (ReasonReact.string("Healthy food"))
                    </a>
                  </li>
                  <li className="main--filter--item">
                    <a className="main--filter--itemLink" href="#">
                      (ReasonReact.string("Pasta"))
                    </a>
                  </li>
                  <li className="main--filter--item">
                    <a className="main--filter--itemLink" href="#">
                      (ReasonReact.string("Veggy"))
                    </a>
                  </li>
                  <li className="main--filter--item-more">
                    <span className="main--filter--item-moreLink">
                      (ReasonReact.string("more"))
                      <img
                        className="main--filter--item-more-triandleDown"
                        src=triandleDown
                      />
                    </span>
                  </li>
                </ul>
              </div>
              <div className="main--content-content">
                <div className="main--content--controlsWrapper">
                  <div className="main--content--controls">
                    <div className="main--content--controls_h">
                      <div className="react-autosuggest__container">
                        <input
                          className="main--content--controlsInput"
                          placeholder="Restaurant name, kitchen or dishes"
                          _type="text"
                        />
                      </div>
                      <i className="fas fa-search icon-search" />
                      <img
                        className="main--promo--adressInputClose-img"
                        src=inputClose
                      />
                    </div>
                  </div>
                </div>
                <div className="main--content--restList">
                  <h2 className="main--content--restList-title">
                    (ReasonReact.string("Restaurants"))
                  </h2>
                  <ul className="main--content--restList-list">
                    <li className="main--content--restItem">
                      <a className="main--content--restItem-link" href="#">
                        <div
                          className="main--content--restItem-imageContainer">
                          <div
                            className="main--content--restItem-imageWrapper">
                            <div
                              className="main--content--restItem-imageNodeRoot">
                              <div
                                className="main--content--restItem-imageNode"
                              />
                            </div>
                          </div>
                        </div>
                        <h3
                          className="main--content--restItem-name"
                          title="Online menu of restaurant McDuck">
                          (ReasonReact.string("McDuck"))
                        </h3>
                        <div className="main--content--restItem-details">
                          <div
                            className="main--content--restItem-deliveryBadge">
                            <div className="deliveryBadge" />
                          </div>
                          <div className="main--content--restItem-rating">
                            <img
                              className="main--content--restItem-starIcon"
                              src=starIcon
                            />
                            (ReasonReact.string(string_of_float(4.9)))
                          </div>
                          <div
                            className="main--content--restItem-orderMinPrice">
                            <img
                              className="main--content--restItem-deliveryPackage"
                              src=deliveryPackage
                            />
                            (ReasonReact.string("Order from "))
                            (ReasonReact.string(string_of_int(0)))
                            (ReasonReact.string(" $"))
                          </div>
                        </div>
                        <div
                          className="main--content--restItem-restTagsWrapper">
                          <div className="main--content--restItem-restTags">
                            <span className="main--content--restItem-restTag">
                              (ReasonReact.string("Russian"))
                            </span>
                            <span className="main--content--restItem-restTag">
                              (ReasonReact.string("$$"))
                            </span>
                          </div>
                        </div>
                      </a>
                    </li>
                  </ul>
                  <div className="main--content--restList-more">
                    <button className="main--content--restList-button">
                      (ReasonReact.string("Show more restaurants"))
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer className="footer--content">
        <div className="footer--footer">
          <div className="footer--copyright">
            (ReasonReact.string("FastfoodDely, 2018"))
          </div>
          <ul className="footer--socials">
            <li className="footer--socialItem">
              <a
                className="footer--socialFB footer--social"
                href="#"
                target="_black">
                <i className="fab fa-facebook-f footer--socialItem" />
              </a>
            </li>
            <li className="footer--socialItem">
              <a
                className="footer--socialVK footer--social"
                href="#"
                target="_black">
                <i className="fab fa-vk footer--socialItem" />
              </a>
            </li>
            <li className="footer--socialItem">
              <a
                className="footer--socialInst footer--social"
                href="#"
                target="_black">
                <i className="fab fa-instagram footer--socialItem" />
              </a>
            </li>
          </ul>
          <div className="footer--links">
            <a className="footer--link" href="#">
              (ReasonReact.string("About us"))
            </a>
            <a className="footer--link" href="#" target="_blank">
              (ReasonReact.string("FAQ"))
            </a>
            <a className="footer--link" href="#" target="_blank">
              (ReasonReact.string("User Agreement"))
            </a>
          </div>
          <div className="footer--feedback">
            <a href="#">
              <i className="fas fa-comment icon-comment" />
              (ReasonReact.string("Feedback"))
            </a>
          </div>
        </div>
      </footer>
    </div>,
};